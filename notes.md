# Gettest notes

En primer lugar, como no aún no había tenido la ocasión de utilizar Foundation, pero si me había interesado varías veces por él he seguido las instrucciones de instalación propuestas por la documentación oficial.
En el directorio adecuado instalo:

> npm install -g bower (para instalar o actualizar y que no surjan conflictos con las dependencias)

> npm install -g bower grunt-cli

> gem install foundation

> foundation new gettest

> gem install bundler

Iniciamos el proyecto  y probamos ambas formas de iniciar el proyecto: Uno:

> bundler
> compass watch

En principio lo realizo en dos directorios distintos ya que se inician con .git y preveo unirlos con posterioridad en otro branch tras un test inicial. Por tanto en un directorio paralelo

> foundation new gettotest --libsass

> cd gettotest

> grunt build

> grunt

Tras realizar algunos ejercicios de *James Stone*, en principio me encuentro más cómodo con la 2ª opción, me parece mejor disponer del *taskrunner* de *Grunt* y obtener las ventajas que tienen los módulos de test de los *scss* que nos ofrece con la instrucción `grunt` en el terminal. Le echamos un vistazo a las variables en los directorios correspondientes (scss):

	.
	├── bower_components
	│   ├── fastclick
	│   ├── foundation
	│   ├── css
	│   ├── js
	│   │   ├── foundation
	│   │   └── vendor
	│   └── scss
	│   │   └── foundation
	│   │       └── components
	│   ├── jquery
	│   ├── jquery-placeholder
	│   ├── jquery.cookie
	│   └── modernizr
	├── css
	├── js
	├── node_modules
	└── scss
----

Tras unos leves cambios empiezo la estructura del html con *Emmet* para comenzar git e iniciar el proyecto con:

> .row>.large-12.columns>ul.breadcrumbs>(li>a>lorem1)*3


A continuación, juego con el framework hasta poder personalizar mis propias clases y posibles mixins, configurando `Sass` para que funcione con la siguiente estructura:

	scss
	├── _settings.scss
	├── app.scss
	└── red-components
	    ├── _red-buttons.scss
	    └── _red-panels.scss
		...


Realizo algunos cambios en los márgenes y distancias y aumento la estructura para completar las dos primeras partes:

	scss
	├── _settings.scss
	├── app.scss
	└── red-components
	    ├── _red-breadcrumbs.scss
	    ├── _red-buttons.scss
	    ├── _red-global.scss
	    └── _red-panels.scss
	    	...

Afortunadamente cuento con `emmet` para resumir "un poco" el html.
Por ejemplo, la tabla se puede resumir así:

table>(thead>(tr>((th>lorem1)*14)))>(tbody>((tr>+((td>lorem1)*14>))*5))
